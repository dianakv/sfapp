//A C C O U N T S  S E R V I C E

describe('AccountsService Unit Tests', function(){

  var utilsMock;

  beforeEach(module('starter.services'));

  beforeEach(function() {
      utilsMock = jasmine.createSpyObj('devUtils', ['readRecords', 'smartSql']);

      module(function($provide) {
          $provide.value('devUtils', utilsMock);
      });
  });



  /* SUCCESS READ RECORDS*/
  describe("readRecords Success", function() {
    beforeEach(inject(function(_$rootScope_, _AccountsService_, _UserService_){
      AccountsService = _AccountsService_;
      UserService = _UserService_;
      $rootScope = _$rootScope_;

    }));


    var options = [];

    it("reads account records when empty", function(done){

      var accounts = [];

      utilsMock.readRecords.and.callFake(function(table, options){
        return new Promise(function(resolve, reject) {
          expect(table).toBe('Account2__ap');
          resolve(accounts);
        });
      });

      AccountsService.all(false).then(function(res){
        expect(res).toBe(accounts);
        expect(res.length).toBe(0);
        // UserService.hasDoneProcess("initialDataLoaded").then(function(res){
        //
        // });
        done();
      });
    });

    it("reads accounts records when not empty", function(done){

      accounts = [{
        'Name': 'Account1',
        'Id': '0020Y00000j27U8QAI',
      },
      {
        'Name': 'Account2',
        'Id': '0020Y00000j27U7QAI'
      }];

      utilsMock.readRecords.and.callFake(function(table, options){
        return new Promise(function(resolve, reject) {
          expect(table).toBe('Account2__ap');
          resolve(accounts);
        });
      });

      AccountsService.all(false).then(function(res){
        expect(res).toBe(accounts);
        expect(res.length).toBe(2);
        done();
      });
    });

  });

});
