
// C O N T A C T S  S E R V I C E


describe('ContactsService Unit Tests', function(){

  beforeEach(module('starter.services'));
  var utilsMock,lnMock;
  beforeEach(function() {
   // Set up our mock Spy object for devUtils calls we are interested in
   utilsMock = jasmine.createSpyObj('devUtils', ['insertRecord', 'smartSql', 'updateRecord']);

   module(function($provide) {
       $provide.value('devUtils', utilsMock);
       $provide.value('LocalNotificationService', lnMock);
   });

 }); // before each

 /* INSERT RECORD - PROMISED SUCCESS */
 describe("insertRecord Success", function() {
   beforeEach(inject(function(_$rootScope_, _ContactsService_){
     ContactsService = _ContactsService_;
     $rootScope = _$rootScope_;
   }));

   it("inserts a record", function(done){
     //set our mock of insertRecord
     utilsMock.insertRecord.and.callFake(function (table, obj) {
       return new Promise(function (resolve, reject) {
         expect(table).toBe('Contact__ap');
         expect(obj.Id).toBe('0030Y00000aj8D4QAI');
         resolve('ok');
       });
     });

     var contact = {Id: '0030Y00000aj8D4QAI'};

     ContactsService.add(contact).then(function(res){
       expect(res).toBe('ok');
       done();
     });

   });

 });

/* PROMISED REJECTION */
describe('promise rejections failure', function(){
  beforeEach(inject(function (_$rootScope_, _ContactsService_) {
    ContactsService = _ContactsService_;
    $rootScope = _$rootScope_;
  }));

//REJECT mock for INSERT
utilsMock.insertRecord.and.callFake(function (table, obj) {
  return new Promise(function(resolve, reject) {
    reject("");
  });
});

});

});
