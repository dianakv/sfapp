
/**
 * Contact Update Controller
 *
 * @description description
 */
(function() {
  'use strict';

  angular
    .module('starter.controllers')
    .controller('ContactUpdateCtrl', ContactUpdateCtrl);

  ContactUpdateCtrl.$inject = ['$stateParams', '$ionicPopup', '$ionicLoading', '$location', 'ContactsService', 'logger'];

  function ContactUpdateCtrl($stateParams, $ionicPopup, $ionicLoading, $location, ContactsService, logger) {

    var vm    = this,
        logTag = "ContactUpdateCtrl";


    //exposed function
    vm.updateContact = updateContact;

    activate();


    function activate(){
      // Request contact details from our ContactsService. We are passing in
      // the contactId, taken from the URL (stateparams) as in our app.js
      ContactsService.get($stateParams.contactId).then(function(contact) {
        vm.c = contact;
      }).catch(function(e) {
        logger.error(logTag, e);
      });
    }

    function updateContact(){
      // Grab the accountId from the parent scope (AccountCtrl)
      vm.c.AccountId = $stateParams.accountId;

      // Make our Name field from our FirstName and LastName fields
      vm.c.Name = vm.c.FirstName + ' ' + vm.c.LastName;

      console.log(logTag, vm.c);

      $ionicLoading.show({
        template: '<p>Updating contact...</p><ion-spinner/>',
        animation: 'fade-in',
        showBackdrop: true,
        duration: 30000,
        delay : 400
      });

      var contact = {
        Id: vm.c.Id,
        AccountId: vm.c.AccountId,
        Name: vm.c.Name,
        Title: vm.c.Title,
        MobilePhone: vm.c.MobilePhone,
        Email: vm.c.Email,
        Department: vm.c.Department,
        Languages__c: vm.c.Languages__c
      };
      ContactsService.updateContact(contact).then(function(res) {
        $ionicLoading.hide();
        $location.path('/tab/account/' + $stateParams.accountId + '/contact/' + $stateParams.contactId);
      }).catch(function(e) {
        logger.error('updateContact failed',e);
        $ionicLoading.hide();
        var alertPopup = $ionicPopup.alert({
          title: 'Update Contact Failed!',
          template: '<p>Sorry, something went wrong.</p><p class="error_details">Error: ' +
            e.status + ' - ' + e.mc_add_status + '</p>'
        });
      });


    }

  }

})();
